FROM		ubuntu:xenial

MAINTAINER	morph027 "morphsen@gmx.com"

RUN	apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get -y -qq install \
		curl \
		checkinstall \
		git-core \
		qtkeychain-dev \
		qt5keychain-dev \
		build-essential \
		cmake \
		doxygen \
		libssl-dev \
		libsqlite3-dev \
		qt5-default \
		pkg-config \
		libqt5webkit5-dev \
		qttools5-dev-tools \
		extra-cmake-modules \
		kio-dev \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

#!/bin/bash

set -e
set -x

PROJECT_DIR=${CI_PROJECT_DIR:-$PWD}

_build() {
[ -z $CI_BUILD_TAG ] && REF=master || REF="v${CI_BUILD_TAG%-*}"
git clone -b "$REF" https://github.com/nextcloud/client_theming
[ -z $CI_BUILD_TAG ] && CI_BUILD_TAG=$(git tag | tail -1 | sed 's/^[^0-9]*//g')
cd client_theming
git submodule update --init --recursive
mkdir build-linux
cd build-linux/
cmake -DCMAKE_INSTALL_PREFIX=/usr -D OEM_THEME_DIR=`pwd`/../nextcloudtheme ../client
sed -i 's/Icon=nextcloud/Icon=Nextcloud/g' src/gui/nextcloud.desktop
sed -i 's/Icon\[\(.*\)\]=nextcloud/Icon\[\1\]=Nextcloud/g' src/gui/nextcloud.desktop
echo 'Nextcloud desktop synchronization client' > description-pak
ncc_maintainer="morph027 <morphsen@gmx.com>"
ncc_name='nextcloud-client'
ncc_version=${CI_BUILD_TAG%-*}
ncc_arch='amd64'
ncc_release=$(date +%Y%m%d)
ncc_license='MIT'
ncc_source='https://github.com/nextcloud/client_theming'
checkinstall -y -D \
--install=no \
--maintainer="$ncc_maintainer" \
--pkgname="$ncc_name" \
--pkgversion="$ncc_version" \
--arch="$ncc_arch" \
--pkgrelease="$ncc_release" \
--pkglicense="$ncc_license" \
--pkgsource="$ncc_source" \
--provides="$ncc_name" \
--requires="libqt5keychain0\|libqt5keychain1,libqt5webkit5,libqt5xml5" \
--exclude='/home' \
make install
}

_build

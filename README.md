# UNMAINTAINED

Please head over to https://launchpad.net/~nextcloud-devs/+archive/ubuntu/client !

# Nextcloud client

![](https://gitlab.com/packaging/nextcloud-client/raw/master/Nextcloud_Logo.png)

This project builds the linux [Nextcloud client](https://github.com/nextcloud/client_theming/) as package using the default instructions and hints from Github user [pasbec](https://github.com/nextcloud/client_theming/issues/25#issuecomment-254232998).

Builds can be found in [Pipelines](https://gitlab.com/packaging/nextcloud-client/pipelines) and in my [repo](https://repo.morph027.de/).

## Currently covered

- Ubuntu Xenial

## easily build your own

Just adjust the docker image to match your operating system and the version tag for nextcloud client.

```bash
mkdir /tmp/docker
docker run --rm -it -v /tmp/docker/:/build -w /build ubuntu:xenial /bin/bash
apt-get update \
&& DEBIAN_FRONTEND=noninteractive apt-get -y -qq install \
curl \
checkinstall \
git-core \
qtkeychain-dev \
qt5keychain-dev \
build-essential \
cmake \
doxygen \
libssl-dev \
libsqlite3-dev \
qt5-default \
pkg-config \
libqt5webkit5-dev \
qttools5-dev-tools \
extra-cmake-modules \
kio-dev \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/*

git clone -b v2.2.4 https://github.com/nextcloud/client_theming
cd client_theming
git submodule update --init --recursive
mkdir build-linux
cd build-linux/
export CMAKE_INSTALL_DIR="/usr"
cmake -DCMAKE_INSTALL_PREFIX=/usr -D OEM_THEME_DIR=`pwd`/../nextcloudtheme ../client
sed -i 's/Icon=nextcloud/Icon=Nextcloud/g' src/gui/nextcloud.desktop
sed -i 's/Icon\[\(.*\)\]=nextcloud/Icon\[\1\]=Nextcloud/g' src/gui/nextcloud.desktop
echo 'Nexcloud desktop synchronization client' > description-pak
ncc_maintainer="morph027 <morphsen@gmx.com>"
ncc_name='nextcloud-client'
ncc_version=$CI_BUILD_TAG
ncc_arch='amd64'
ncc_release=$(date +%Y%m%d)
ncc_license='MIT'
ncc_source='https://github.com/nextcloud/client_theming'
checkinstall -y -D \
--install=no \
--maintainer="$ncc_maintainer" \
--pkgname="$ncc_name" \
--pkgversion="$ncc_version" \
--arch="$ncc_arch" \
--pkgrelease="$ncc_release" \
--pkglicense="$ncc_license" \
--pkgsource="$ncc_source" \
--provides="$ncc_name" \
--requires="libqt5keychain0"
--exclude='/home' \
make install
```
